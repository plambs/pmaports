# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland
pkgver=5.52.0
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all"
url="https://www.kde.org"
license="LGPL-2.1"
depends=""
depends_dev="qt5-qtbase-dev wayland-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/${pkgname}-${pkgver}.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring running Wayland compositor

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DKDE_INSTALL_LIBEXECDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="db36b42e491bf25faf45aa0c5b6b924558e8472f616ae70132bf48ca30fc620ce090690e01111d6cdd01ad3b9b0dbed45f1685c28aa67cb337e9eeb66575098c  kwayland-5.52.0.tar.xz"
